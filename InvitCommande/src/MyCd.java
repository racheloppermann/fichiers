import java.io.File;

public class MyCd {

	/**
	 * M�thode qui permet de se d�placer dans un r�pertoire ou r�pertoire pr�cedent
	 * @param args1 chemin du r�pertoire
	 * @param repActuelle
	 * @param systeme
	 * @return
	 */
	public static String cd(String args1[], String repActuelle, String systeme) {
		if (args1[0] == null) {
			repActuelle = System.getProperty("user.dir");
		} else if (args1[0].equals("..")) {
			File rep = new File(repActuelle);
			String parent = rep.getParent();
			repActuelle = parent;
			System.out.println(parent);
		} else {
			String[] dir = new java.io.File(repActuelle).list();
			for (int i = 0; i < dir.length; i++) {
				if ((args1[0]).equals("/" + dir[i])) {
					if (systeme.equals("Windows")) {
						int positionRemplacee = 0;
						char nouveauChar = '\\';
						args1[0] = args1[0].substring(0, positionRemplacee) + nouveauChar
								+ args1[0].substring(positionRemplacee + 1);
					}
					repActuelle = repActuelle + args1[0];
					break;
				}
				if (i == dir.length - 1) {
					System.out.println(args1[0] + " Aucun fichier ou dossier de ce type");
				}
			}
		}
		
		return repActuelle;
	}

}
