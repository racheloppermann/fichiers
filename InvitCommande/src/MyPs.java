import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyPs {

	/**
	 * M�thode qui permet d'afficher les processus en cours
	 * @param systeme
	 * @param repActuelle
	 * @throws IOException
	 */
	public static void ps(String systeme, String repActuelle) throws IOException {
		if (systeme.equals("Windows")) {
			try {
			    String line;
			    Process p = Runtime.getRuntime().exec
			    	    (System.getenv("windir") +"\\system32\\"+"tasklist.exe");
			    BufferedReader input =
			            new BufferedReader(new InputStreamReader(p.getInputStream()));
			    while ((line = input.readLine()) != null) {
			        System.out.println(line); //<-- Parse data here.
			    }
			    input.close();
			} catch (Exception err) {
			    err.printStackTrace();
			}
		}
		if (systeme.equals("Linux")) {

			String proc = "/proc/";
			repActuelle = proc;
			String[] dir = new java.io.File(repActuelle).list();
			Pattern patternDossier = Pattern.compile("[0-9]+");
			String[] tab = new String[300];
			int j = 0;

			for (int i = 0; i < dir.length; i++) {
				Matcher matcherDossier = patternDossier.matcher(dir[i]);
				if (matcherDossier.find()) {
					tab[j] = dir[i];
					j++;
				}
			}

			for (int k = 0; k < tab.length; k++) {
				if (tab[k] != null) {
					BufferedReader lecteurAvecBuffer = null;
					String ligne;

					try {
						lecteurAvecBuffer = new BufferedReader(new FileReader("/proc/" + tab[k] + "/status"));
					} catch (FileNotFoundException exc) {
						System.out.println("Erreur d'ouverture");
					}

					Pattern patternName = Pattern.compile("Name+.*");
					Pattern patternState = Pattern.compile("State+.*");
					Pattern patternPid = Pattern.compile("^Pid+.*");
					Pattern patternVmSize = Pattern.compile("VmSize+.*");
					Pattern patternVmRSS = Pattern.compile("VmRSS+.*");
					String name = null;
					String state = null;
					String pid = null;
					String size = null;
					String rss = null;

					while ((ligne = lecteurAvecBuffer.readLine()) != null) {

						Matcher matcherName = patternName.matcher(ligne);
						Matcher matcherState = patternState.matcher(ligne);
						Matcher matcherPid = patternPid.matcher(ligne);
						Matcher matcherVmSize = patternVmSize.matcher(ligne);
						Matcher matcherVmRSS = patternVmRSS.matcher(ligne);

						if (matcherName.find()) {
							name = matcherName.group().trim();
						} else if (matcherState.find()) {
							state = matcherState.group().trim();
						} else if (matcherPid.find()) {
							pid = matcherPid.group().trim();
						} else if (matcherVmSize.find()) {
							size = matcherVmSize.group().trim();
						} else if (matcherVmRSS.find()) {
							rss = matcherVmRSS.group().trim();
						}

					}
					System.out.println(name + "\t\t" + state + "\t\t" + pid + "\t\t" + size + "\t\t" + rss);
					lecteurAvecBuffer.close();
				} else {
					System.out.println("Fini !");
					break;
				}
			}
		}

	}

}
