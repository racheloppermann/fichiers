import java.io.File;

public class MyLs {

	/**
	 * M�thode qui permet d'afficher le contenu d'un dossier (avec ou sans les permissions)
	 * @param args1 l'option de la commande (l)
	 * @param repActuelle
	 */
	public static void ls(String args1[], String repActuelle) {
		if (args1[0] == null) {
			String[] dir = new java.io.File(repActuelle).list();
			for (int i = 0; i < dir.length; i++) {
				System.out.println(dir[i]);
			}
		} else {
			if (args1[0].equals("l")) {
				String[] dir = new java.io.File(repActuelle).list();
				String read = "";
				String write = "";
				String exe = "";
				for (int i = 0; i < dir.length; i++) {
					File dossierFichier = new File(dir[i]);
					if (dossierFichier.canWrite() == true) {
						write = "w";
					} else {
						write = "-";
					}
					if (dossierFichier.canExecute() == true) {
						exe = "x";
					} else {
						exe = "-";
					}
					if (dossierFichier.canRead() == true) {
						read = "r";
					} else {
						read = "-";
					}
					System.out.println(read + write + exe + " " + dir[i]);
				}
			} else {
				System.out.println("Commande incorrect");
			}
		}

	}

}
