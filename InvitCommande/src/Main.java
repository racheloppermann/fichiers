import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

	public static void main(String[] args) throws IOException {
		List<String> listCommandes = new ArrayList<String>();
		listCommandes.add("my-pwd : Afficher le répertoire actuel");
		listCommandes.add("my-ps : Afficher les processus en cours");
		listCommandes.add("my-ls : Afficher le contenu d'un dossier");
		listCommandes.add("my-ls l : Afficher le contenu d'un dossier avec ses permissions");
		listCommandes.add("my-display NomDuFichier.txt : Afficher le contenu du fichier demandé");
		listCommandes.add("my-cd .. : Se déplacer dans le répertoire précédent");
		listCommandes.add("my-cd /dossier : Se déplacer dans un répertoire");
		listCommandes.add(
				"my-mv RépertoireOùSeTrouveLeFichier/NomDuFichier.txt RépertoireOùDéplacerLeFichier/NomDuFichier.txt : Déplacer un fichier et/ou le renommer");
		listCommandes.add(
				"my-cp RépertoireOùSeTrouveLeFichierACopier/NomDuFichier.txt RépertoireOùCollerLeFichier/NomDuFichier.txt : Copier un fichier et le renommer");
		listCommandes.add("my-help : Afficher la liste des commandes");
		listCommandes.add("my-rm NomDuFichier.txt : Supprimer le fichier demandé");
		listCommandes.add(
				"my-find RépertoireOùChercher FichierATrouver.txt : Chercher un fichier dans le répertoire demandé");
		listCommandes.add(
				"my-find -R RépertoireOùChercher FichierATrouver.txt : Chercher un fichier dans le répertoire demandé et ses sous-répertoires");
		listCommandes.add("my-quit : Quitter le programme");
		Scanner sc = new Scanner(System.in);
		String repActuelle = System.getProperty("user.dir");
		boolean arret = false;
		System.out.println("Sur quel système d'exploitation vous trouvez-vous (Linux, Windows) ?");
		String systeme = null;
		boolean sExploitation = false;
		while (!sExploitation) {
			systeme = sc.nextLine();
			switch (systeme) {
			case "Linux": {
				sExploitation = true;
				break;
			}
			case "Windows": {
				sExploitation = true;
				break;
			}
			default:
				System.out.println("Saisie invalide, veuillez choisir parmi les systèmes proposés");
			}
		}

		while (!arret) {
			System.out.println(repActuelle + ">");
			String saisie = sc.nextLine();
			Pattern patternCommande = Pattern.compile("^my-[a-z]*");
			Pattern patternArg1 = Pattern.compile("\\s+\\S+");
			Matcher matcherCommande = patternCommande.matcher(saisie);
			Matcher matcherArg1 = patternArg1.matcher(saisie);
			String commande = "";
			String[] args1 = new String[10];
			int t = 0;
			while (matcherCommande.find()) {
				commande = matcherCommande.group();
			}
			while (matcherArg1.find()) {
				args1[t] = matcherArg1.group().trim();
				t++;
			}

			switch (commande) {
			case "my-quit": {
				System.out.println("fin programme");
				arret = true;
				System.exit(1);
				break;
			}

			case "my-ls": {
				MyLs.ls(args1, repActuelle);
				break;
			}

			case "my-ps": {
				MyPs.ps(systeme, repActuelle);
				break;
			}

			case "my-rm": {
				MyRm.rm(args1);
				break;
			}

			case "my-cp": {
				MyCp.cp(args1);
				break;
			}

			case "my-mv": {
				MyMv.mv(args1);
				break;
			}

			case "my-find": {
				MyFind.find(args1);
				break;
			}

			case "my-cd": {
				repActuelle = MyCd.cd(args1, repActuelle, systeme);
				break;
			}

			case "my-pwd": {
				System.out.println("Vous vous trouvez dans le répertoire :");
				System.out.println(repActuelle);
				break;
			}

			case "my-display": {
				MyDisplay.display(args1);
				break;
			}

			case "my-help": {
				System.out.println("Liste des commandes disponibles :");
				for (String element : listCommandes) {
					System.out.println(element);
				}
				break;
			}

			default:
				System.out.println("Saisie invalide, tapez my-help pour voir les commandes proposées.");
			}

		}
	}

}