import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class MyCp {

	/**
	 * M�thode qui permet de copier un fichier (et de le renommer si besoin)
	 * @param args1 args1[0] le chemin/fichier � copier args1[1] chemin/nomfichier � coller
	 */
	public static void cp(String args1[]) {
		File fichierACopier = new File(args1[0]);
		File fichierAColler = new File(args1[1]);
		try {
			Files.copy(fichierACopier.toPath(), fichierAColler.toPath(), StandardCopyOption.REPLACE_EXISTING);
			System.out.println("Je copie le fichier : " + fichierACopier + " en " + fichierAColler);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
