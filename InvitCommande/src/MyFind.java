import java.io.File;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MyFind {

	/**
	 * M�thode qui permet de chercher un fichier dans le r�pertoire demand� ou chercher un fichier dans le r�pertoire demand� et ses sous-r�pertoires"
	 * @param args1 args1[0] l'endoit o� l'onse chercher args1[1] le fichier � trouver
	 */
	public static void find(String args1[]) {
		if ("-R".equals(args1[0])) {
			File fichierATrouver = new File(args1[1]);
			Pattern patternFind = Pattern.compile("^.*" + args1[2] + ".*$");
			findByRegexR(fichierATrouver, patternFind);
		} else {
			findByRegex(args1[0], args1[1]);
		}

	}
	
	/**
	 * M�thode qui permet de chercher un fichier dans le r�pertoire demand� et ses sous-r�pertoires
	 * @param folder l'endoit o� l'onse chercher
	 * @param filename le fichier � trouver
	 * @return
	 */
	public static String findByRegexR(File folder, Pattern filename) {
		String path = null;
		if (folder.isDirectory()) {
			List<File> files = Arrays.asList(folder.listFiles());
			Iterator<File> fileIterator = files.iterator();
			while (fileIterator.hasNext() && path == null) {
				path = findByRegexR(fileIterator.next(), filename);
			}
		} else {
			Matcher matcher = filename.matcher(folder.getName().toLowerCase());
			if (matcher.find()) {
				path = folder.getAbsolutePath();
				System.out.println("�l�ment trouv� : " + path);
				path = null;
			}
		}
		return path;
	}

	/**
	 * M�thode qui permet de chercher un fichier dans le r�pertoire demand�
	 * @param folder l'endoit o� l'onse chercher
	 * @param filename le fichier � trouver
	 * @return
	 */
	public static String findByRegex(String folder, String filename) {
		String[] dir = new java.io.File(folder).list();
		for (int i = 0; i < dir.length; i++) {
			if (dir[i].equals(filename)) {
				System.out.println("�l�ment trouv� : " + dir[i]);
			}
		}
		return folder;
	}

}
