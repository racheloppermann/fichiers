import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MyDisplay {

	/**
	 * M�thode qui permet d'afficher le contenu d'un fichier
	 * @param args1 args1[0] le fichier � lire
	 */
	public static void display(String args1[]) {
		try {
			String fichier = args1[0];
			InputStream flux = new FileInputStream(fichier);
			InputStreamReader lecture = new InputStreamReader(flux);
			BufferedReader buff = new BufferedReader(lecture);
			String ligne;
			while ((ligne = buff.readLine()) != null) {
				System.out.println(ligne);
			}
			buff.close();
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}

}
