import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class MyMv {

	/**
	 * M�thode qui permet de d�placer et/ou renommer un fichier
	 * @param args1 args1[0] args1[0] le chemin/fichier � d�placer args1[1] chemin/nomfichier � coller
	 */
	public static void mv(String args1[]) {
		File fichierACouper = new File(args1[0]);
		File fichierAColler = new File(args1[1]);
		try {
			Files.copy(fichierACouper.toPath(), fichierAColler.toPath(), StandardCopyOption.REPLACE_EXISTING);
			fichierACouper.delete();
			System.out.println("Je d�place le fichier : " + fichierACouper + " � " + fichierAColler);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
