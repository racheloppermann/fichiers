import java.io.File;
import java.util.Scanner;

public class MyRm {

	/**
	 * M�thode qui permet de supprimer un fichier
	 * @param args1 le fichier � supprimer 
	 */
	public static void rm(String args1[]) {
		System.out.println("Etes-vous sur de vouloir supprimer le fichier :" + args1[0]);
		System.out.println("A : OUI \t B : NON");
		String menu = "";
		Scanner sc = new Scanner(System.in);
		menu = sc.nextLine();
		switch (menu) {
		case "A": {
			try {
				File file = new File(args1[0]);
				if (file.delete()) {
					System.out.println(file.getName() + " a �t� supprim�.");
				} else {
					System.out.println("Op�ration de suppression echou�e");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		}

	}

}
